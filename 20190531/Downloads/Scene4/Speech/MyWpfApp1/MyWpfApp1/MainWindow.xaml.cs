﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/*
using System.ComponentModel;
using System.Runtime.CompilerServices;
*/
using System.Diagnostics;
using System.Globalization;
using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Translation;

namespace MyWpfApp1
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    
    public partial class MainWindow : Window//, INotifyPropertyChanged
    {
        /*
        #region Events

        /// <summary>
        /// Implement INotifyPropertyChanged interface
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Helper function for INotifyPropertyChanged interface
        /// </summary>
        /// <typeparam name="T">Property type</typeparam>
        /// <param name="caller">Property name</param>
        private void OnPropertyChanged<T>([CallerMemberName]string caller = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(caller));
            }
        }

        #endregion Events
        */

        //#region Helper functions
        public MainWindow()
        {
            this.InitializeComponent();
            //this.InitializeVoiceMap();
        }

        private void SetCurrentText(TextBlock textBlock, string text)
        {
            this.Dispatcher.Invoke(
                () =>
                {
                    textBlock.Text = text;
                });
        }

        /// <summary>
        /// Raises the System.Windows.Window.Closed event.
        /// </summary>
        /// <param name="e">An System.EventArgs that contains the event data.</param>
        /*
        protected override void OnClosed(EventArgs e)
        {
            if (this.config != null)
            {
                this.recognizer.Dispose();
                this.config = null;
            }

            base.OnClosed(e);
        }
        */
        /// <summary>
        /// Logs the recognition start.
        /// </summary>
        private void LogRecognitionStart(TextBox log, TextBlock currentText)
        {
            string recoSource = "microphone";
            this.SetCurrentText(currentText, string.Empty);
            log.Clear();
            this.WriteLine(log, "\n--- Start speech translation using " + recoSource + " ----\n\n");
        }

        /// <summary>
        /// Writes the line.
        /// </summary>
        private void WriteLine(TextBox log)
        {
            this.WriteLine(log, string.Empty);
        }

        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        private void WriteLine(TextBox log, string format, params object[] args)
        {
            var formattedStr = string.Format(CultureInfo.InvariantCulture, format, args);
            Trace.WriteLine(formattedStr);
            Dispatcher.Invoke(() =>
            {
                log.AppendText((formattedStr + "\n"));
                log.ScrollToEnd();
            });
        }

        //#endregion

        private TranslationRecognizer recognizer;
        private SpeechTranslationConfig config;
        private bool started;

        public string Region { get; set; }

        /// <summary>
        /// Gets or sets From Language
        /// </summary>
        public string FromLanguage { get; set; }

        /// <summary>
        /// Gets or sets To Languages
        /// </summary>
        public List<string> ToLanguages { get; set; }
        /*
        public string SubscriptionKey
        {
            get
            {
                return this.subscriptionKey;
            }

            set
            {
                this.subscriptionKey = value?.Trim();
                this.OnPropertyChanged<string>();
            }
        }
        */
        private async void StartButtonClick(object sender, RoutedEventArgs e)
        {

            //this.settingsPanel.IsEnabled = false;

            // if (string.IsNullOrEmpty(this.SubscriptionKey))
            
            if (string.IsNullOrEmpty(this.subkey.Text))
            {
                this.WriteLine(this.crisLogText, "Subscriptionkey is NULL");
            }

            //this.Region = ((ComboBoxItem)regionComboBox.SelectedItem).Tag.ToString();         
            this.Region = "japaneast";
            //this.FromLanguage = ((ComboBoxItem)fromLanguageComboBox.SelectedItem).Tag.ToString();
            this.FromLanguage = "ja-Jp";
            this.ToLanguages = new List<string>();
            //this.VoiceLanguage = ((ComboBoxItem)voiceComboBox.SelectedItem).Tag.ToString();

            string languageCode = "en-US";
            if (!this.ToLanguages.Contains(languageCode))
            {
                this.ToLanguages.Add(languageCode);
            }
            //if (this.subscriptionKey == null || this.subscriptionKey.Length <= 0)
            if (this.subkey == null || this.subkey.Text.Length <= 0)
            {
                MessageBox.Show("Subscription Key is wrong or missing!");
                this.WriteLine(this.crisLogText, "--- Error : Subscription Key is wrong or missing! ---");
                this.WriteLine(this.crisLogText, this.Region.ToString());
                return;
            }

            if (!this.started)
            {
                this.started = true;
                this.LogRecognitionStart(this.crisLogText, this.crisCurrentText);
                this.CreateRecognizer();
                await Task.Run(async () => { await this.recognizer.StartContinuousRecognitionAsync().ConfigureAwait(false); });
            }
        }

        private async void StopButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.started)
            {
               // this.settingsPanel.IsEnabled = true;
                await Task.Run(async () => { await this.recognizer.StopContinuousRecognitionAsync().ConfigureAwait(false); });
                this.started = false;
            }
        }

        private void CreateRecognizer()
        {
            //    this.config = SpeechTranslationConfig.FromSubscription(SubscriptionKey, Region);
            this.config = SpeechTranslationConfig.FromSubscription(this.subkey.Text, Region);
            this.config.SpeechRecognitionLanguage = FromLanguage;
            //this.config.VoiceName = voice;
            ToLanguages.ForEach(l => this.config.AddTargetLanguage(l));

            this.recognizer = new TranslationRecognizer(this.config);

        }

        private void OnRecognizingEventHandler(object sender, TranslationRecognitionEventArgs e)
        {
            string text = e.Result.Text;
            foreach (var t in e.Result.Translations)
            {
                text += $"\nSame in {t.Key}: {t.Value}";
            }

            this.SetCurrentText(this.crisCurrentText, text);
        }

        #region Recognition Event Handlers

        private void OnRecognizedEventHandler(object sender, TranslationRecognitionEventArgs e)
        {
            if (e.Result.Text.Length == 0)
            {
                this.WriteLine(this.crisLogText, "Reason: " + e.Result.Reason);
                this.WriteLine(this.crisLogText, "No phrase response is available.");
            }
            else
            {
                string text = e.Result.Text;
                foreach (var t in e.Result.Translations)
                {
                    text += $"\nSame in {t.Key}: {t.Value}";
                }

                this.SetCurrentText(this.crisCurrentText, text);
                text += "\n";
                this.WriteLine(this.crisLogText, text);
            }
        }

        private void OnCanceledEventHandler(object sender, TranslationRecognitionCanceledEventArgs e)
        {
            string text = $"Speech recognition: canceled. Reason: {e.Reason}, ErrorDetails: {e.ErrorDetails}";
            this.SetCurrentText(this.crisCurrentText, text);
            text += "\n";
            this.WriteLine(this.crisLogText, text);
            if (this.started)
            {
                this.recognizer.StopContinuousRecognitionAsync().Wait();
                this.started = false;
            }
        }



        #endregion

    }
}
